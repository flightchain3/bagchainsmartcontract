"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BagDataHelper {
    static serializeBag(bagInfo) {
        return Buffer.from(JSON.stringify(bagInfo));
    }
    static serializePrivateData(paxInfo) {
        return Buffer.from(JSON.stringify(paxInfo.passengers));
    }
    static bufferToObject(buffer) {
        if (buffer === null) {
            return null;
        }
        const bufferString = buffer.toString('utf8');
        if (bufferString.length <= 0) {
            return null;
        }
        try {
            console.log('returning buffer: ' + bufferString);
            return JSON.parse(bufferString);
        }
        catch (err) {
            console.error('Error parsing buffer to JSON', bufferString);
            return null;
        }
    }
    static async iteratorToHistory(iterator) {
        const results = [];
        let res;
        while (true) {
            res = await iterator.next();
            console.log('Found something: ' + res.value);
            if (res.value && res.value.value) {
                const bag = this.bufferToObject(res.value.value);
                console.log('Bag: ' + bag);
                const parsedItem = {
                    bagData: bag,
                    key: res.value.key,
                    timestamp: res.value.timestamp,
                };
                results.push(parsedItem);
            }
            if (res.done) {
                await iterator.close();
                break;
            }
        }
        return results;
    }
}
exports.BagDataHelper = BagDataHelper;
