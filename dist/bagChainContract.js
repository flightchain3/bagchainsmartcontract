"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fabric_contract_api_1 = require("fabric-contract-api");
const bagChainUtil_1 = require("./bagChainUtil");
const bagConverter_1 = require("./bagConverter");
const bagDataHelper_1 = require("./bagDataHelper");
const bagKeyValidator_1 = require("./bagKeyValidator");
const paxConverter_1 = require("./paxConverter");
class BagChainContract extends fabric_contract_api_1.Contract {
    constructor() {
        super(...arguments);
        this.PASSENGER_COLLECTION_NAME = 'passengerCollection';
    }
    async initLedger(ctx) {
        console.info('=================== START: Initialize Ledger =====================');
    }
    async getVersion(ctx) {
        console.info('===================    START: getVersion    =====================');
        return new Promise((resolve, reject) => {
            resolve(process.env.npm_package_version);
        });
    }
    async getBag(ctx, bagKey) {
        console.info('===================   START:  getBag    =====================');
        bagKeyValidator_1.BagKeyValidator.validateBagkey(bagKey);
        const rawData = await ctx.stub.getState(bagKey);
        return bagDataHelper_1.BagDataHelper.bufferToObject(rawData);
    }
    async getBagHistory(ctx, bagKey) {
        console.info('===================   START:  getBagHistory    =====================');
        bagKeyValidator_1.BagKeyValidator.validateBagkey(bagKey);
        const rawData = await ctx.stub.getHistoryForKey(bagKey);
        return await bagDataHelper_1.BagDataHelper.iteratorToHistory(rawData);
    }
    async getBagQuery(ctx, query) {
        console.info('===================   START:  getBagQuery    =====================');
        const rawData = await ctx.stub.getQueryResult(query);
        return await bagDataHelper_1.BagDataHelper.iteratorToHistory(rawData);
    }
    async saveBag(ctx, bagJson) {
        console.info('===================   START:  saveBag    =====================');
        const bagInfo = bagConverter_1.BagConverter.convert(bagJson);
        const privateData = ctx.stub.getTransient();
        const pax = paxConverter_1.PaxConverter.convert(privateData);
        const newKey = bagChainUtil_1.BagChainUtil.generatebagKey(bagInfo);
        await ctx.stub.putState(newKey, bagDataHelper_1.BagDataHelper.serializeBag(bagInfo));
        if (pax && bagInfo) {
            console.info('Private Data Found');
            if (pax.passengers.length > 0) {
                await ctx.stub.putPrivateData(this.PASSENGER_COLLECTION_NAME, bagChainUtil_1.BagChainUtil.generatebagKey(bagInfo), bagDataHelper_1.BagDataHelper.serializePrivateData(pax));
            }
        }
    }
}
exports.BagChainContract = BagChainContract;
