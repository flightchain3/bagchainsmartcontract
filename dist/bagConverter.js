"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bagInfoValidator_1 = require("./bagInfoValidator");
class BagConverter {
    static convert(json) {
        let bagInfo = null;
        const bag = JSON.parse(json);
        if (bagInfoValidator_1.BagInfoValidator.validate(bag)) {
            bagInfo = bag;
        }
        else {
            throw new Error('Invalid JSON Entered');
        }
        return bagInfo;
    }
}
exports.BagConverter = BagConverter;
