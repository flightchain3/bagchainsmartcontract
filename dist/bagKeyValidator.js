"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BagKeyValidator {
    static validateBagkey(bagKey) {
        if (bagKey.length === 0) {
            throw new Error('missing bag key - cannot get Bag data');
        }
        if (isNaN(parseInt(bagKey))) {
            throw new Error('Bag key is not a valid number');
        }
        if (parseInt(bagKey) < 0) {
            throw new Error('Bag Key cannot be a minus number ');
        }
    }
}
exports.BagKeyValidator = BagKeyValidator;
