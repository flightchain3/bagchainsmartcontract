"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const paxInfoValidator_1 = require("./paxInfoValidator");
class PaxConverter {
    static convert(map) {
        let paxInfo = null;
        if (!map.get('passengers')) {
            return paxInfo;
        }
        else {
            const paxBuffer = map.get('passengers');
            const pax = JSON.parse(paxBuffer.toString('utf8'));
            if (!pax || pax === null
                || pax.passengers.length === 0) {
                return null;
            }
            else {
                if (paxInfoValidator_1.PaxInfoValidator.validate(pax)) {
                    paxInfo = pax;
                }
                return paxInfo;
            }
        }
    }
}
exports.PaxConverter = PaxConverter;
