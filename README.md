#Bag Chain Smart Contract

### getting started
1. Copy files from laptop to shared folder inside VM box

open devices->Shared Folders->Shared Folder Settings
check "Folder Name" - e.g. vb

replace vb with whatever is set as "Folder name"

```
sudo mount -t vboxsf vb /media/vb
```

2. Copy under flightchain folder
 
copy under ~/flightchain/flightchainsmartcontract from the shared folder

3. install dependencies (optionally run gulp to check all is ok)

```
npm install
gulp 			
```

4. docker exec into cli node

```
docker exec -it cli bash
```

see peer command reference: https://hyperledger-fabric.readthedocs.io/en/latest/commands/peercommand.html


5. install the chaincode on each peer node

first, peer0:

```
export CORE_PEER_ADDRESS=peer0.sandbox.sita.aero:7051

peer chaincode install -n bagchain -v 1.0 -l node -p /opt/src/node/bagchainsmartcontract
```

to check:

```
peer chaincode list --installed
```

second, peer1:

```
export CORE_PEER_ADDRESS=peer1.sandbox.sita.aero:7051

peer chaincode install -n bagchain -v 1.0 -l node -p /opt/src/node/bagchainsmartcontract
```

6. instantiate the chaincode
```
peer chaincode instantiate -o orderer.sita.aero:7050 -C channel-flight-chain -n bagchain -v 1.0 -l node -c '{"Args":["BagChainContract:initLedger"]}' -P "OR ('SITAMSP.member','Org2MSP.member')" --collections-config /opt/src/node/bagchainsmartcontract/privateData/collectionConfig.json
```

to check:

```
peer chaincode list --instantiated -C channel-flight-chain
```

OR (on the host - not inside cli)

``` 
docker ps 
```

7. Query the chaincode
```
peer chaincode query -C channel-flight-chain -n bagchain -c '{"Args":["getVersion"]}'
```

should return "1.0.0" or whatever version is in package.json

``` 
peer chaincode query -C channel-flight-chain -n bagchain -c '{"Args":["getBag","2019-08-230123456789"]}'
```

8. Invoke the chaincode
```
peer chaincode invoke -o orderer.sita.aero:7050 -C channel-flight-chain -n bagchain -c '{"Args":["saveBag", "{\"bagTag\":\"0123456789\",\"locationInfo\": {\"description\": \"T1-DUB\", \"geocoordinates\": \"53.349804, -6.260310\"},\"flight\":{\"departureDate\":\"2019-08-23\"}}"]}' 
```

### Upgrading Smart contract

See steps 1-5 from before - except update the version number

6. upgrade

```
git pull
gulp compile

docker exec -it cli bash

export CORE_PEER_ADDRESS=peer0.sandbox.sita.aero:7051
peer chaincode install -n bagchain -v 1.1 -l node -p /opt/src/node/bagchainsmartcontract

export CORE_PEER_ADDRESS=peer1.sandbox.sita.aero:7051
peer chaincode install -n bagchain -v 1.1 -l node -p /opt/src/node/bagchainsmartcontract

peer chaincode upgrade -o orderer.sita.aero:7050 -C channel-flight-chain -n bagchain -v 1.1 -l node -c '{"Args":["BagChainContract:initLedger"]}' -P "OR ('SITAMSP.member','Org2MSP.member')" --collections-config /opt/src/node/bagchainsmartcontract/privateData/collectionConfig.json
```

### Deploying to kubernetes environment
### Steps for deploying into kubernetes dashboard demo for blockchain

1.  shh in cli-sitaeu pod

2.  get bagchain inside cluster using git clone
```
git clone https://gitlab.com/flightchain3/bagchainsmartcontract.git
```

3. get dependencies and compile typescript to javascript using gulp
```
cd bagchainsmartcontract
npm i
npm i - g gulp-cli
gulp compile
```

4. install chaincode onto 1st peer
```
export CORE_PEER_ADDRESS=peer1st-sitaeu.fabric12:7051
peer chaincode install -n bagchain -v 1.0 -p /opt/gopath/src/github.com/hyperledger/fabric/peer/bagchainsmartcontract -l node
```

5. repeat for 2nd peer
```
export CORE_PEER_ADDRESS=peer2nd-sitaeu.fabric12:7051
peer chaincode install -n bagchain -v 1.0 -p /opt/gopath/src/github.com/hyperledger/fabric/peer/bagchainsmartcontract -l node
```

6. instantiate chaincode
```
export ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fabric12/orderers/orderer1st-sita.fabric12/msp/tlscacerts/tlsca.fabric12-cert.pem 
peer chaincode instantiate -o orderer1st-sita.fabric12:7050 --channelID sitatranchannel -n bagchain -l node -v 1.0 -c '{"Args":["BagChainContract:initLedger"]}' -P "OR ('sitaeuMSP.member','sitausMSP.member')" --tls --cafile=$ORDERER_CA --collections-config /opt/gopath/src/github.com/hyperledger/fabric/peer/bagchainsmartcontract/privateData/collectionConfig.json
```
Note: if a version of bagchain is already deployed, just replace instantiate with upgrade

7. verify deployment
```
peer chaincode list --instantiated --channelID "sitatranchannel"
```



