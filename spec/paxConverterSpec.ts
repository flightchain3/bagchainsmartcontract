import { PaxInfoValidator } from '../src/paxInfoValidator';
import { PaxConverter } from '../src/paxConverter';

describe('BagConverter', () => {
    describe('convert', () => {
        it('should return valid paxinfo object when invalid json is supplied', () => {
            spyOn(PaxInfoValidator, 'validate').and.throwError('Validation failed');   //Set Validator Outcome To True
            const paxString = {
                passengers: [{
                    fullname: "Tom Doherty"
                }]
            };

            const mockTransientMap = new Map<string, Buffer>();
            const paxBuffer = Buffer.from(JSON.stringify(paxString));
            mockTransientMap.set('passengers', paxBuffer)

            try {
                PaxConverter.convert(mockTransientMap);
                fail('This passed?? - TEST FAILED!!')
            } catch (ex) {
                expect(ex.message).toEqual('Validation failed');
            }
        });

        it('should return valid baginfo object when valid json is supplied', () => {
            spyOn(PaxInfoValidator, 'validate').and.returnValue(true);   //Set Validator Outcome To True
            const paxString = {
                passengers: [{
                    fullname: "Tom Doherty"
                }]
            };

            const mockTransientMap = new Map<string, Buffer>();
            const paxBuffer = Buffer.from(JSON.stringify(paxString));
            mockTransientMap.set('passengers', paxBuffer)

            const result = PaxConverter.convert(mockTransientMap);

            expect(result.passengers[0].fullname).toEqual('Tom Doherty');

        });

        it('should return null when no passenger object is supplied', () => {
            spyOn(PaxInfoValidator, 'validate').and.returnValue(true)
            const paxString = {
                passengers: []
            };
            const mockTransientMap = new Map<string, Buffer>();
            const paxBuffer = Buffer.from(JSON.stringify(paxString));
            mockTransientMap.set('passengers', paxBuffer)

            const result = PaxConverter.convert(mockTransientMap);

            expect(result).toEqual(null);
        });
    });
});

