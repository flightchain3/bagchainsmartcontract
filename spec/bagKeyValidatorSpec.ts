const bagChainContract = require('../src/bagChainContract');


describe('BagKeyValidator', () => {
    describe('validateBagKey', () => {
        it('should not call getState when bagKey is null', async () => {
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };
            mockCtx.stub.getState.and.returnValue(Buffer.from('{"some":"json"}'));
            const contract = new bagChainContract.BagChainContract();
            try {
                await contract.getBag(mockCtx, "");
                expect(mockCtx.stub.getState).not.toHaveBeenCalled();
            } catch (ex) {
                expect(ex.message).toEqual('missing bag key - cannot get Bag data');
            }
        });

        it('should not call getState when invalid bagKey is supplied', async () => {
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };
            mockCtx.stub.getState.and.returnValue(Buffer.from('{"some":"json"}'));
            const contract = new bagChainContract.BagChainContract();
            try {
                await contract.getBag(mockCtx, "bagKey");
                expect(mockCtx.stub.getState).not.toHaveBeenCalled();
            } catch (ex) {
                expect(ex.message).toEqual('Bag key is not a valid number');
            }
        });

        it('should not call getState when bagKey is a minus number', async () => {
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };
            mockCtx.stub.getState.and.returnValue(Buffer.from('{"some":"json"}'));
            const contract = new bagChainContract.BagChainContract();
            try {
                await contract.getBag(mockCtx, "-123456");
                expect(mockCtx.stub.getState).not.toHaveBeenCalled();
            } catch (ex) {
                expect(ex.message).toEqual('Bag Key cannot be a minus number ');
            }
        });
    });
});