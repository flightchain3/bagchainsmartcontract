import { BagInfoValidator } from '../src/bagInfoValidator';

describe('BagInfoValidator', () => {
    describe('validate', () => {
        it('should return true if all the data is correct in the json', () => {
            const result = BagInfoValidator.validate({
                "bagTag": "1234567890", "operatorID": "987", "locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": { "flightNumber": "5118", "departureDate": "2018-09-05", "destinationAirport": "Terminal A - LND"}});  
            expect(result).toEqual(true);
        });

        it('should return false if bagTag is not a number', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "ABCDEFGH12", "locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": {"flightNumber": "5118", "departureDate": "2019", "destinationAirport": "Terminal B - BA" } });
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid bag data, bag tag not a valid number.');
            }
        });

        it('should return false if bagTag length is not equal to 10 digits', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "123456789", "locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": {"flightNumber": "5118", "departureDate": "2019", "destinationAirport": "Terminal B - BA" } });
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid bag data, bag tag not a valid length.');
            }
        });

        it('should return false if departureDate is not a valid date', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "1234567890", "locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": { "departureDate": "ABC123", "destinationAirport": "Terminal A - LND", "flightNumber": "5118" } });
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, departureDate is not a valid date.');
            }
        });


        it('should return false if flightNumber is not valid', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "1234567890", "locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": { "departureDate": " 2011-10-12", "destinationAirport": "Terminal A - LND", "flightNumber": "ABC" } });
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid flight data, flightNumber is invalid.');
            }
        });


        it('should return false if operatorID is not a number', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "0123456789", "operatorID": "XYZ", "locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": {"departureDate": "2018-09-05"}});
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('OperatorID is not a valid number.');
            }
        });

        it('should return false if geo-coordinate is not valid', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "1234567890", "operatorID": "12345", "locationInfo": {"description": "DUB", "geocoordinates": "100100100"}, "flight": { "flightNumber": "5118",  "departureDate": "2018-09-05", "destinationAirport": "Terminal A - LND" }});
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('Geo-Coordinate format is invalid.');
            }
        });


        it('should return false if ULD ID is not valid', () => {
            try {
                const result = BagInfoValidator.validate({ "bagTag": "0123456789" ,"locationInfo": {"description": "DUB", "geocoordinates": "53.349804, -6.260310"}, "flight": {"departureDate": "2018-09-05"}, "operatorID": "123", "uldID": "BadULD"});
                fail('did not throw expected error');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid bag data, ULD ID is invalid.');
            }
        });
        
    });
});

