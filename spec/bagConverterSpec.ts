import { BagInfoValidator } from '../src/bagInfoValidator';
import {BagConverter} from '../src/bagConverter';

describe('BagConverter', () => {
    describe('convert', () => {
        it('should return valid baginfo object when valid json is supplied', () => {
            spyOn(BagInfoValidator, 'validate').and.returnValue(true);   //Set Validator Outcome To True
            
            const result = BagConverter.convert('{"bagTag": "0123456789", "locationInfo": {"destinationAirport": "test", "geocoordinates": "02728"}, "flight":{"flightNumber": "BA1234"}}');
            expect(result.bagTag).toEqual("0123456789");
        });

       it('should return null when invalid json is supplied', () => {
            spyOn(BagInfoValidator, 'validate').and.throwError('Invalid JSON Entered');
            
            try {
                BagConverter.convert('{"some": "invalidjson", "locationInfo": {"destinationAirport": "test", "geocoordinates": "02728"}, "departureDate": "2019","flightNumber": "BA997"}');
                fail('Unexpected Success When Passing Invalid JSON');
            } catch (ex) {
                expect(ex.message).toEqual('Invalid JSON Entered');
            }
        });
        
    });
});

