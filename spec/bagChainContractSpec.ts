const bagChainContract = require('../src/bagChainContract');
import { BagConverter } from '../src/bagConverter';
import { BagInfoValidator } from '../src/bagInfoValidator';
import { PaxConverter } from '../src/paxConverter';
import { IBagInfo } from '../src/model/IBagInfo';
import { IPaxInfo } from '../src/model/IPaxInfo';

describe('bagChainContract', () => {
    describe('saveBag', () => {
        it('should call putstate when valid json is supplied', () => {
            const bagInfo = {
                bagTag: "0123456789",
                locationInfo: {
                    description: "DUB",
                    geocoordinates: "53.349804, -6.260310"
                },
                flight: {
                    flightNumber: "AK1234",
                    departureDate: "2019-08-08"
                }
            };
            const paxInfo = {
                passengers: [{
                    fullname: "Tom Doherty"
                }]
            };

            const bagKey = bagInfo.flight.departureDate + bagInfo.bagTag;
            spyOn(BagConverter, 'convert').and.returnValue(bagInfo as IBagInfo);
            spyOn(PaxConverter, 'convert').and.returnValue(paxInfo as IPaxInfo);

            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };
            const mockTransientMap = new Map<string, any>();
            mockTransientMap.set('passengers', Buffer.from(JSON.stringify(paxInfo)));
            mockCtx.stub.getTransient.and.returnValue(mockTransientMap);

            const bagContract = new bagChainContract.BagChainContract();

            bagContract.saveBag(mockCtx, bagInfo);
            expect(mockCtx.stub.putState).toHaveBeenCalledWith(bagKey, jasmine.any(Buffer));

        });


        it('should not call putState when invalid json is supplied', async () => {
            spyOn(BagInfoValidator, 'validate').and.throwError('something went wrong converting the json');
            spyOn(BagConverter, 'convert').and.throwError('something went wrong converting the json');
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };

            const contract = new bagChainContract.BagChainContract();

            try {
                await contract.saveBag(mockCtx, '{"destinationAirport": "Terminal B - BA", "departureDate": "2019", "flightNumber": "BA997"}');
                expect(mockCtx.stub.putState).not.toHaveBeenCalled();
            } catch (ex) {
                expect(ex.message).toEqual('something went wrong converting the json');
            }
        });
    });

    describe('getBag', () => {
        it('should call getstate when valid json is supplied', async () => {
            const bagInfo = {
                bagTag: "0",
                locationInfo: {
                    description: "DUB",
                    geocoordinates: "036872"
                },
                flight: { destinationAirport: "Terminal B - BA", departureDate: "2019", flightNumber: "BA997" }
            };

            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };
            spyOn(BagConverter, 'convert').and.returnValue(bagInfo as IBagInfo);
            mockCtx.stub.getState.and.returnValue(Buffer.from('{"some":"json"}'));

            const contract = new bagChainContract.BagChainContract();
            contract.getBag(mockCtx, "2019-07-250123456789");
            expect(mockCtx.stub.getState).toHaveBeenCalledWith("2019-07-250123456789");
        });


        it('should not call getState when invalid json is supplied', async () => {
            const mockCtx = { stub: jasmine.createSpyObj('stub', ['getState', 'getTxID', 'putState', 'putPrivateData', 'getTransient']) };
            mockCtx.stub.getState.and.throwError('something went wrong converting the json')

            const contract = new bagChainContract.BagChainContract();
            try {
                await contract.getBag(mockCtx, "0123456789");
                expect(mockCtx.stub.getState).not.toHaveBeenCalled();
            } catch (ex) {
                expect(ex.message).toEqual('something went wrong converting the json');
            }
        });

    });
});