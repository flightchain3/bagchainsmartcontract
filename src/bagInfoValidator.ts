import { IBagInfo } from './model/IBagInfo';

export class BagInfoValidator {
    public static validate(bagObject: IBagInfo) {

        if (!bagObject.bagTag || !bagObject.locationInfo.description || !bagObject.flight.departureDate) {
            const msg = 'Invalid bag data.BagTag, location or departureDate not suppplied.';
            console.warn(msg, bagObject);
            throw new Error(msg);
        }

        if (isNaN(parseInt(bagObject.bagTag)) || (parseInt(bagObject.bagTag)) < 0) {
            const msg = 'Invalid bag data, bag tag not a valid number.';
            console.warn(msg, bagObject.bagTag);
            throw new Error(msg);
        }

        if (bagObject.bagTag.length !== 10) {
            const msg = 'Invalid bag data, bag tag not a valid length.';
            console.warn(msg, bagObject.bagTag);
            throw new Error(msg);
        }

        if (bagObject.flight && bagObject.flight.departureDate) {
            if (isNaN(Date.parse(bagObject.flight.departureDate))) {
                const msg = 'Invalid flight data, departureDate is not a valid date.';
                console.warn(msg, bagObject.flight.departureDate);
                throw new Error(msg);
            }
        }

        if (bagObject.flight && bagObject.flight.flightNumber) {
            if (!bagObject.flight.flightNumber.match(/^[0-9]{2,4}?$/g)) {
                const msg = 'Invalid flight data, flightNumber is invalid.';
                console.warn(msg, bagObject.flight.flightNumber);
                throw new Error(msg);
            }
        }

        if (bagObject.operatorID) {
            if (isNaN(parseInt(bagObject.operatorID)) || (parseInt(bagObject.operatorID) < 0
                || !bagObject.operatorID)) {
                const msg = 'OperatorID is not a valid number.';
                console.warn(msg, bagObject.operatorID);
                throw new Error(msg);
            }
        }

        if (bagObject.locationInfo && bagObject.locationInfo.geocoordinates === ',') {
            const newGeo = '31.143333, 121.805275';
            bagObject.locationInfo.geocoordinates = newGeo;
            console.warn('NEW GEO: ', bagObject.locationInfo.geocoordinates);
        }

        if (bagObject.locationInfo && bagObject.locationInfo.geocoordinates &&
            !bagObject.locationInfo.geocoordinates.match(/^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/g)) {
            const msg = 'Geo-Coordinate format is invalid.';
            console.warn(msg, bagObject.locationInfo.geocoordinates);
            throw new Error(msg);

        }

        if (bagObject.uldID) {
            if (!bagObject.uldID.match(/^[A-Z]{3}[0-9]{4,5}[A-Z]{2,3}?$/g)) {
                const msg = 'Invalid bag data, ULD ID is invalid.';
                console.warn(msg, bagObject.uldID);
                throw new Error(msg);
            }
        }

        return true;
    }

}
