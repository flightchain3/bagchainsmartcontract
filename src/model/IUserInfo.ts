export interface IUserInfo {
    type?: string;
    fullname?: string;
    company?: string;
}
