export interface IFlightInfo {
    carrierCode?: string;
    flightNumber?: string;
    departureDate: string;
    departureTime?: string;
    departureAirport?: string;
    destinationAirport?: string;
}
