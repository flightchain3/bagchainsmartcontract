import { IActionInfo } from './IActionInfo';
import { IEventInfo } from './IEventInfo';
import { IFlightInfo } from './IFlightInfo';
import { ILocationInfo } from './ILocationInfo';
import { IUserInfo } from './IUserInfo';

export interface IBagInfo {
    bagTag: string;
    locationInfo: ILocationInfo;
    operatorID?: string;
    uldID?: string;
    baggageCarousel?: string;
    flight?: IFlightInfo;
    events?: IEventInfo;
    actions?: IActionInfo;
    user?: IUserInfo;
}
