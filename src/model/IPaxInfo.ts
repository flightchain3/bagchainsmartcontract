export interface IPaxInfo {
    passengers?: [
        {
            fullname?: string;
            seatNumber?: string;
            status?: string;
            seqNum?: string;
            signature?: string;
        }
    ];
}
