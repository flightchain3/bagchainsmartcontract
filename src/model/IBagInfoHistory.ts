import {IBagInfo} from './IBagInfo';

export interface IBagInfoHistory {
    bagData: IBagInfo;
    timestamp: object;
    key: string;
}
