import { BagInfoValidator } from './bagInfoValidator';
import { IBagInfo } from './model/IBagInfo';

export class BagConverter {

  public static convert(json: string): IBagInfo {
    let bagInfo: IBagInfo = null;
    const bag = JSON.parse(json);

    if (BagInfoValidator.validate(bag)) {
      bagInfo = bag as IBagInfo;
    } else {
      throw new Error('Invalid JSON Entered');
    }

    return bagInfo;
  }
}
