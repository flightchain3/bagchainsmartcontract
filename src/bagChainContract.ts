import { Context, Contract } from 'fabric-contract-api';
import { BagChainUtil } from './bagChainUtil';
import { BagConverter } from './bagConverter';
import { BagDataHelper } from './bagDataHelper';
import { BagKeyValidator } from './bagKeyValidator';
import { PaxConverter } from './paxConverter';

export class BagChainContract extends Contract {

    private PASSENGER_COLLECTION_NAME = 'passengerCollection';

    public async initLedger(ctx: Context) {
        console.info('=================== START: Initialize Ledger =====================');
    }

    public async getVersion(ctx: Context): Promise<string> {
        console.info('===================    START: getVersion    =====================');
        return new Promise<string>((resolve, reject) => {
            resolve(process.env.npm_package_version);
        });
    }

    public async getBag(ctx: Context, bagKey: string) {
        console.info('===================   START:  getBag    =====================');
        BagKeyValidator.validateBagkey(bagKey);
        const rawData = await ctx.stub.getState(bagKey);
        return BagDataHelper.bufferToObject(rawData);
    }

    public async getBagHistory(ctx, bagKey: string) {
        console.info('===================   START:  getBagHistory    =====================');
        BagKeyValidator.validateBagkey(bagKey);
        const rawData = await ctx.stub.getHistoryForKey(bagKey);
        return await BagDataHelper.iteratorToHistory(rawData);
    }

    public async getBagQuery(ctx, query: string) {
        console.info('===================   START:  getBagQuery    =====================');
        const rawData = await ctx.stub.getQueryResult(query);
        return await BagDataHelper.iteratorToHistory(rawData);
    }

    public async saveBag(ctx: Context, bagJson: string) {
        console.info('===================   START:  saveBag    =====================');
        const bagInfo = BagConverter.convert(bagJson);

        const privateData = ctx.stub.getTransient();
        const pax = PaxConverter.convert(privateData);

        const newKey = BagChainUtil.generatebagKey(bagInfo);
        await ctx.stub.putState(newKey, BagDataHelper.serializeBag(bagInfo));

        if (pax && bagInfo) {
            console.info('Private Data Found');
            if (pax.passengers.length > 0) {
                await ctx.stub.putPrivateData(this.PASSENGER_COLLECTION_NAME,
                    BagChainUtil.generatebagKey(bagInfo), BagDataHelper.serializePrivateData(pax));

            }

        }
    }

}
