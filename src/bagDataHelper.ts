import { Iterators } from 'fabric-shim';
import { IBagInfo } from './model/IBagInfo';
import { IBagInfoHistory } from './model/IBagInfoHistory';
import { IPaxInfo } from './model/IPaxInfo';

export class BagDataHelper {

    public static serializeBag(bagInfo: IBagInfo): Buffer {
        return Buffer.from(JSON.stringify(bagInfo));
    }

    public static serializePrivateData(paxInfo: IPaxInfo): Buffer {
        return Buffer.from(JSON.stringify(paxInfo.passengers));
    }

    public static bufferToObject(buffer: Buffer): IBagInfo {
        if (buffer === null) {
            return null;
        }

        const bufferString = buffer.toString('utf8');
        if (bufferString.length <= 0) {
            return null;
        }

        try {
            console.log('returning buffer: ' + bufferString);
            return JSON.parse(bufferString);
        } catch (err) {
            console.error('Error parsing buffer to JSON', bufferString);
            return null;
        }
    }

    public static async iteratorToHistory(iterator: Iterators.HistoryQueryIterator):
        Promise<IBagInfoHistory[]> {
        const results = [];

        let res;
        while (true) {
            res = await iterator.next();
            console.log('Found something: ' + res.value);
            if (res.value && res.value.value) {
                const bag = this.bufferToObject(res.value.value);
                console.log('Bag: ' + bag);
                const parsedItem: IBagInfoHistory = {
                    bagData: bag,
                    key: res.value.key,
                    timestamp: res.value.timestamp,
                };
                results.push(parsedItem);
            }
            if (res.done) {
                await iterator.close();
                break;
            }
        }

        return results;
    }
}
