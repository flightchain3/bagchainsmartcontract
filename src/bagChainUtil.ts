import { IBagInfo } from './model/IBagInfo';

export class BagChainUtil {

    public static getCurrentDate() {
        const today = new Date();
        const dd = String(today.getDate()).padStart(2, '0');
        const mm = String(today.getMonth() + 1).padStart(2, '0');
        const yyyy = today.getFullYear();
        const currentDate = yyyy + '-' + mm + '-' + dd;
        return currentDate;
    }

    public static generatebagKey(bagInfo: IBagInfo) {

        let bagKey = ' ';
        if (bagInfo.flight && bagInfo.flight.departureDate) {
            bagKey = bagInfo.flight.departureDate + bagInfo.bagTag;
        } else {
            bagKey = this.getCurrentDate() + bagInfo.bagTag;
        }
        console.warn('bagKey: ' + bagKey);
        return bagKey;
    }

}
