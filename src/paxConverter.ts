import { IPaxInfo } from './model/IPaxInfo';
import { PaxInfoValidator } from './paxInfoValidator';

export class PaxConverter {

  public static convert(map: Map<string, Buffer>): IPaxInfo {
    let paxInfo: IPaxInfo = null;

    if (!map.get('passengers')) {
      return paxInfo;
    } else {
      const paxBuffer = map.get('passengers');
      const pax = JSON.parse(paxBuffer.toString('utf8'));
      if (!pax || pax === null
        || pax.passengers.length === 0) {
        return null;
      } else {

        if (PaxInfoValidator.validate(pax)) {
          paxInfo = pax as IPaxInfo;
        }

        return paxInfo;
      }
    }
  }
}
